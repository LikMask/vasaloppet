//******************************************************************
// Programmerare: Robin Will�n wilrob-2
// Datum: 2016-02-05
// Senast uppdaterad: 2016-02-06 Robin Will�n
// Beskrivning: This application is for people how what to calculate the time it take to do vasaloppet, they set the time of day when they pass the finish line  
// and compare the time whit J�rgen Brink 2012 (winner and current record holder on vasaloppet)
// It is required that the user enter the time he/she finish the race
// Version: 0.2 Change the if statement.
//****************************************************************** 

package vasaloppet;

import java.util.Scanner;

public class Vasaloppet {

	public static void main(String[] args) {
		
		//Declaring variables
		
		String delimit = "------------------------------------------------------------";
		
		//Declaring variables: Record time for completing vasloppet 2012
		
		int recordHour = 3;
		int recordMinute = 38;
		int recordSecond = 41;

		//Declaring variables: Difference between record time and the user
		
		int diffSecond = 0;
		int diffMinute = 0;
		int diffHour = 0;

		//Declaring variables: Input variables from user
		
		int hour = 0;
		int minute = 0;
		int second = 0;
		
		//Input from user
		
		Scanner in = new Scanner(System.in);
		
		System.out.println("Vasaloppet - Mata in m�lg�ngstid");
		System.out.println(delimit);
		System.out.print("Timme:      ");
		hour = in.nextInt();
		System.out.print("Minut:      ");
		minute = in.nextInt();
		System.out.print("Sekund:     ");
		second = in.nextInt();

		in.close();
		
		//Printing users time
		
		System.out.println("Din tid var:           " + (hour - 8) + " timmar " + minute + " minuter och " + second + " sekunder");
		System.out.println(delimit);

		//Printing record time
		
		System.out.println("Rekordtiden 2012 var:  " + recordHour + " timmar " + recordMinute + " minuter och " + recordSecond + " sekunder");
		System.out.println(delimit);
		
		//Calculations: find out how come first
		
		recordSecond = recordSecond + (recordMinute * 60) + (recordHour * 3600);
		second = second + (minute * 60) + ((hour - 8 ) * 3600);
		if(second>recordSecond)
		{
			diffSecond = second - recordSecond;
		}
		else
		{
			diffSecond = recordSecond - second; 
		}

		//Calculations: convert seconds to hours, minutes and seconds 
		
		while (diffSecond  >= 3600)
		{
			diffHour++;
			diffSecond = diffSecond - 3600;
		}
		while (diffSecond >= 60)
		{
			diffMinute++;
			diffSecond = diffSecond - 60;
		}
		
		//Printing difference between the record and the users time
		
		System.out.println("Skillnaden i tid �r:   " + diffHour + " timmar " + diffMinute + " minuter och " + diffSecond + " sekunder");
		
	}

}